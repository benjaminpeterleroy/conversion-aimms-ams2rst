

LibraryModule Library AimmsXLLibrary
####################################

.. aimms:librarymodule:: Library_AimmsXLLibrary

    :attribute Prefix: ``axll``

    :attribute Interface: :doc:`PublicSection`


    This library allows you to read from and write to .xlsx or .xls (Excel) files.
    
    The library does not need Excel to be installed on the machine and works both in Windows and Linux.
    
    The library can only read and write the file formats .xlsx and .xls, but is not capable of 
    evaluating any formula or macro that is contained in it. For that you need Excel itself.
    
    The functions in this library do not use a return value to indicate success or failure.
    Instead, the functions are created to be used in combination with the error handling mechanisms in AIMMS.
    That is why it is highly recommended to place all function calls within a 'block-onerror-endblock' context,
    so that you can easily handle the warnings and errors that might occur during the usage of these
    functions.
    
    A typical usage looks like:
    
      .. code::
    
        block

          axll::OpenWorkbook("mybook.xlsx");
      
          ! .. read or write the sheets in the workbook ..
      
        onerror err do
      
          ! .. handle the error or warning ..
      
          errh::MarkAsHandled(err);
      
        endblock;
      
        axll::CloseAllWorkbooks;  ! save and close any open workbook
.. aimms:module:: axll

.. toctree::

   PublicSection
   