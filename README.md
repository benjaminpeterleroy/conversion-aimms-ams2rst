Python script to convert files in AMS format to files in RST format, ready for AIMMS documentation

How to use it
----------------

Open a command prompt in the folder where the script is located, and go for:

``python ConvertAMS2RST.py Your_AMS_File.ams``

The output ``.rst`` file will be written in a `docs` subfolder from where this script is located.


Requirements
-------------

- Python 3


Build the docs
----------------

1. **Install the minimal set up**


   In order to build the HTML docs out of the RST output, you will need to install the [sphinx python package](https://www.sphinx-doc.org/en/master/index.html), as well as the [AIMMS theme for sphinx](https://gitlab.com/ArthurdHerbemont/sphinx-aimms-theme/-/tree/master/) by running:

   ``` 
   python -m pip install sphinx sphinx-aimms-theme

   ```

2. **Run the build**

    Browse to the `docs` folder, and run from the command line:
    
    ``` 
    python -msphinx . _build/html
    ```

3. **Look at the result**

    Browse to `<root>/docs/_build/html/`

    **Tip** : To avoid the sphinx warning "duplicate object description of ...", you may remove the _build folder before building by running

    ```
    python -msphinx -M clean . _build/html
    ``` 

Play with options
-------------------

[Options at the beginning of the script](ConvertAMS2RST.py#L13) can be changed

- **skip_undocumented_id** : if ``True``, skip any identifier that doesn't contain any comment, except if it contains nested identifier containing comments
- **attribute_list_to_ignore**: A list of potential identifier attributes you would like to ignore. For example, 'DllName' or 'Encoding'.
